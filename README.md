
## Install

git clone https://github.com/torch/distro.git ~/build/torch --recursive

sudo apt-get install -y build-essential curl \
     cmake libreadline-dev git-core libqt4-dev libjpeg-dev \
     libpng-dev ncurses-dev imagemagick libzmq3-dev gfortran \
     unzip gnuplot gnuplot-x11 ipython libopenblas-dev

./install.sh

## copy files from char-rnn

* util/
* model/
* train.lua
* sample.lua

## CUDA

```bash
sudo apt install nvidia-cuda-dev nvidia-cuda-toolkit

# compiling cunn requires gcc 5
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 1
luarocks install cunn
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 1
```

ao correr o luarocks install cunn (depois tb teria de instalar o cutorch), ele dava-me erro de versão errada do gcc. Depois de 

sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 1

deu-me [outro erro](https://askubuntu.com/questions/923319/compiling-torch-on-ubuntu-17-04-no-support-for-gcc-version-5-and-gcc-error-gc) que indicava que precisava do g++-5, mas nos repos só há o 6... por isso desisti.

Há a hipótese de usar o OpenCL, por isso tentei

luarocks install clnn

mas também deu problemas (Please install cltorch from https://github.com/hughperkins/distro-cl)

# Referências

* <http://torch.ch/docs/getting-started.html>
* <https://dwijaybane.wordpress.com/2017/07/25/setup-char-rnn-and-torch-rnn-for-character-level-language-model-in-torch/>
* <https://github.com/karpathy/char-rnn>

# Usar

```bash
# set up Torch
. /home/rlafuente/build/torch/install/bin/torch-activate
```
