#!/usr/bin/env python3
import random
import subprocess
from utils import post_to_twitter

cmd = ['th', 'sample.lua', 'cv/lm_lstm_epoch73.17_3.7450.t7',
       '-seed', str(random.randint(10, 10000)),
       '-length', '400',
       '-verbose', '0'
       ]

def clean_line(line):
    if line.endswith('-'):
        line = line.rstrip('-')
    return line.replace('"', '').replace('(', '').replace(')', '')

def get_verses():
    output = subprocess.run(cmd, capture_output=True, encoding="iso-8859-1").stdout
    # partir o resultado numa lista
    lines = output.strip().split('\n')[1:5]
    # tenta repetidamente até não haver erros de encoding
    try:
        # consertar o encoding
        return [line.encode('iso-8859-1').decode('utf-8') for line in lines]
    except UnicodeDecodeError:
        # erro? Há um carácter estranho, gerar outra estrofe e ver se dá.
        return get_verses()

lines = get_verses()

# eliminar parênteses e aspas
lines = [clean_line(line) for line in lines]

# assegurar que a última linha termina com um ponto final
last_line = lines[-1]
if last_line.endswith(('.', ',', ';', ':')):
    last_line = last_line[:-1]
last_line += '.'
lines[-1] = last_line

content = "\n".join(lines)

post_to_twitter(content)
