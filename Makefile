LOAD_VENV_CMD=. `pwd`/.env/bin/activate

install:
	virtualenv .env --python=/usr/bin/python3
	$(LOAD_VENV_CMD); pip install -r requirements.txt

tweet:
	$(LOAD_VENV_CMD); ./estrofe.sh
