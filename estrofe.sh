#!/bin/bash
source ~/build/torch/install/bin/torch-activate
source .env/bin/activate
# run command but kill it after 10secs if it's still running
timeout 10 python3 estrofe.py
